* "**Backend**" and "**Frontend**" represent two different projects that may exist within a product.
* "**Shared Libraries**" **1** and **2** represent source code, that is used both in "Backend" and "Frontend". 

**Frontend** has three directories &mdash; "src", "lib1" and "lib2". 
* "Src" is the code, specific for this project.  
* "Lib1" is a special directory, that is synced with another repository &mdash; "Shared Library 1". All changes made in "Shared Library 1" will be immediately implemented in this directory. This is called an **X-Module**.
* "Lib2" is also an X-Module &mdash; a directory, synced with "Shared Library 2".

![2020-07-05_19-53-25|493x414](https://support.tmatesoft.com/uploads/default/optimized/1X/e80a1ee785df01d86ca18c7e4f34feabe3c83d88_2_1035x621.png) 

All commits to a repository, synced with an X-Module (e.g. "Shared Library 1") are reflected in the commit history of the X-Modules repository ("Frontend").  Note, that different X-Modules may have different merge strategies &mdash; it affects, how the commits from the external repository are represented.  Read more about it [here](https://support.tmatesoft.com/t/merge-strategies-for-modules-subtree-merge-rebase-and-squash/2360). 

![2020-07-05_19-56-42|689x273](https://support.tmatesoft.com/uploads/default/optimized/1X/abcda950d8cb3c4b9b7bb93427586619d1c9ce41_2_1033x409.png) 